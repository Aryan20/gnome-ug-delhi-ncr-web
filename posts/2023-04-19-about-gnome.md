---
layout: layouts/post.njk
title: "About GNOME"
description: What is GNOME? Is it used for only garden decoration, or is there another GNOME which is changing how we use our computers? Let's explore.
date: 2023-04-19T12:30:00.000Z
featuredImage: https://in.gnome.org/images/uploads/GnomeLogoHorizontal.svg
---
# Hello, all!!

Today we'll learn about what is, GNOME.

This is a question a lot of people get, and what better way to answer that than by creating a blog?

So, let's goooooo.

##### **First things first**
We are NOT talking about these things -

![A picture of a garden GNOME holding a flower pot](https://in.gnome.org/images/uploads/garden_gnome_scaled.jpg)

They might be cute (for some 😅), but that is not what we're going to talk about.
We'll be discussing the GNU Network Object Model Environment. Yes, it is a mouthful. Now you know why we call it GNOME :) Easier to say and better.

##### **Second thing**
The pronunciation. It is the little things that spice up the talks in the community, like the pronunciation of GNOME. 

Some like to spell it Guh-NOME, some like NOME, and some say Ge-NOME. 

It is said that the original and official way was supposed to be Guh-NOME, but the community has gone past that, and no one really cares. What matters is the message. But yeah, definitely a communication starter or ender 🙃.

![Some dude giving funny expressions and saying "What did you say?"](https://in.gnome.org/images/uploads/anchorman-what.gif)

##### **Okay, but what is GNOME?**
So, this is a bit hard, for some it is a community, for some it is an organization, and for some it is the keeper of peace, protector of the realm and the 7 continents :D

But in my perspective, it is a bit of all. It is a volunteer-driven mission, encouraging FOSS by delivering great quality software products for ALL.

![GIF of a man saying "Did you say free" with funny and weird expressions](https://in.gnome.org/images/uploads/free.gif)

The biggest of them is the GNOME desktop environment, created using a combination of various open-source projects, built by both, GNOME and others.

![Screenshot of GNOME DE taken from GNOME website](https://in.gnome.org/images/uploads/gnome_de.jpg)

GNOME Also provides - GTK (GNOME/GIMP Tool Kit), using which developers can make apps which look right at home with GNOME. And with the addition of LibAdwaita, they can also look great on mobile devices. Yes, GNOME is working on a mobile shell as well :D

Examples of GNOME apps ->

![Screenshot of Software center of GNOME](https://in.gnome.org/images/uploads/software.png)
![Screenshot of GIMP photo editing program](https://in.gnome.org/images/uploads/gimp.png)
![Screenshot of Nautilus, the default file manager of GNOME](https://in.gnome.org/images/uploads/nautilus.png)
![Screenshot of GNOME settings application](https://in.gnome.org/images/uploads/settings.png)

##### **What is a Desktop Environment(DE)?**
Simply put, it is the GUI you interact with. With MS Windows and Mac OS, you don't get a choice for the look and feel, everything is bundled together. But, in the case of Linux, you have the option.



Linux-based OS by default has no GUI, but we have plenty of organizations making one, again, Linux OS is a combination of the Linux Kernel and various software, one of them can be the DE too.

There are plenty of options to choose from. Some of the most famous are - GNOME, KDE Plasma, Cinnamon, and XFCE. But there are a lot more options available.

##### **So why GNOME?**
Well... that is a personal preference, and you have to find your reason to stick around. For the writer, they are - The awesome community, the clean interface, the vision and the feel of the products.

It offers an interface which can please most people, hence they are the default choice for major distributions like - Ubuntu, Debian, Fedora, etc...

But, again, this is the beauty of this space, you need not use GNOME, you can still be a part of this community and enjoy the space. No one DE has hate for the other. It is a place where we all learn and grow together.

##### **The END**
That's the end, all I would recommend is that you try it on your own, and see if you like it. I tried a lot of the available DE, even used to hate GNOME at one point, but eventually got around to loving it the most.

Written by - Aryan Kaushik