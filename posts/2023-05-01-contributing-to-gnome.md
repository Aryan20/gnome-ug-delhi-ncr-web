---
layout: layouts/post.njk
title: "Contributing to GNOME"
description: Ever wondered about how to contribute to GNOME? Let's explore it together.
date: 2023-05-01T17:15:00.000Z
featuredImage: https://in.gnome.org/images/uploads/GnomeLogoHorizontal.svg
---

#  Hola! Mucho gusto (Nope, this is not a Spanish blog 😉)
Have you ever used GNOME or heard about it and thought how can I make it even better? How can I contribute to its success? What can I give back to the community?

Well, then you are at the right place 😃

Today we'll explore the ways in which we can do just that.

##### **Why should I contribute?**
As you probably know, open source relies on volunteer work. It is this spirit that powers this whole community.

It is the feeling of pride and happiness you gain after helping others and the fight for better and open products that fuel the energy for all the volunteers.

Not every user can contribute, and no one is asking for it, but if you have the capacity to do it then it won't only help others but will come back to you as well as you are a user too.

##### **I'm not technical, so I can't right?**
Well... that is not true. Technical contributions are just a part of the big picture.

Most open-source projects desperately need good documentation, so good writers are always welcome, after all, developing or using software without proper documentation is hard

Apart from documentation, the community is a really big if not the biggest part of the mission. Thus a community manager, event coordinator, marketer, and just anyone that loves the idea and would love to keep the community healthy and strong is really needed.

And last but definitely not least, UI/UX developers are really important, no one wants to use technology that looks archaic, and no one likes to look at posters and videos which are all over the place and doesn't deliver the message.

If you fall in the above categories, then the [Engagement team](https://wiki.gnome.org/Engagement) and [GNOME design team](https://wiki.gnome.org/Design) are probably the perfect place for you, apart from contributions to individual projects.

##### **Want to do technical contributions?**
Now, this is my area of interest, so let's get started.

GNOME and products relying on its technologies are written in a variety of languages with most using - C, Vala, Rust, and Python.

GNOME uses its own design toolkits, known as - Gtk (GNOME Tool Kit) and now LibAdwaita as well, making applications work perfectly on all screen sizes. 

These toolkits ensure that all applications written for GNOME look at home and blend well.

Thus for most of the software of GNOME, if you touch the graphical part, then these libraries will definitely be needed.

Apart from these, we have libraries for specific tasks, they are - Pango (Text manipulation), il8n (For translation), Graphene (A thin layer of mathematical types for 3D libraries), GObject (The base type system library), GIO (GObject Interfaces and Objects, Networking, IPC, and I/O), Gdk (The GTK windowing system abstraction), Gsk (The GTK rendering abstraction)

Their documentation can be accessed on the Gtk4 docs page.

[GTK4 Documentation](https://docs.gtk.org/gtk4/)

[LibAdwaita Documentation](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/)

##### **Gtk Bindings**
Gtk has binding in most languages you might want to use, including but not limited to -

**All** - [About all the bindings](https://www.gtk.org/docs/language-bindings/)

**C** - [GTK4 Documentation](https://docs.gtk.org/gtk4/)

**Vala** - [Vladoc](https://valadoc.org/gtk4/Gtk.html)

**Rust** - [Gtk-rs](https://gtk-rs.org/)

**Python** - [PyGObject](https://pygobject.readthedocs.io/en/latest/)

**JavaScript** - [GJS](https://gjs-docs.gnome.org/)

##### **So, how to start (Non - Technical)**
You can join the engagement team and design team Matrix groups and see where it takes you - #gnome-design:gnome.org, #engagement:gnome.org

##### **So, how to start (Technical)**
You can join [GNOME GitLab](https://gitlab.gnome.org) as GNOME uses its own hosted GitLab instance for contributions.

There you can search for projects, go to the issues page and select the label "newcomers" or "GoodFirstIssues", etc...

These issues are generally easier to solve and would let you break into the workflow.

You can then pick an issue and drop a message to check if it is available, or take context from previous messages on the issue, if any.

And that's it, if you can fix it, do that and make a PR 😄

Remember to add SSH authentication, or you will get an "I'm a teapot" error while pushing lol, yeah, I was just as confused as you 🙂.

##### **Some projects to contribute to**

For Python lovers, my favourite project 😄 - [Pitivi](https://gitlab.gnome.org/GNOME/pitivi), etc...

For C lovers, there are a lot of really good projects, some of them are - [GNOME Settings](https://gitlab.gnome.org/GNOME/gnome-control-center), [Nautilus](https://gitlab.gnome.org/GNOME/nautilus), etc...

For Vala lovers - [GNOME Boxes](https://gitlab.gnome.org/GNOME/gnome-boxes), etc...

For Rusties - [Amberol](https://gitlab.gnome.org/World/amberol), etc...

For JavaScripters - GNOME Extensions, and some projects.

For web devs - [Faces of GNOME](https://gitlab.gnome.org/Teams/Engagement/websites/faces-of-gnome), [GNOME Extensions](https://gitlab.gnome.org/Infrastructure/extensions-web)

For UI/UX - [GNOME HIG CSS](https://gitlab.gnome.org/Teams/Engagement/websites/gnome-css-library)

Remember this list is very small, as there are a ton of projects, and typing all of them is hard 😉

##### **The End**
Well, that's all, I hope you got some insights and would help improve this space even further.

See you in the next one.

Written by - Aryan Kaushik
