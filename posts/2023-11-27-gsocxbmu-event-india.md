---
layout: layouts/post.njk
title: GSoCxBMU event India
description: "About the recent GSoCxBMU event held at BMU "
date: 2023-11-27T15:26:00.000Z
featuredImage: https://www.aryank.in/images/uploads/copy-of-copy-of-foss-is-967-×-332px-.png
---
# Namaste Everyone!

This is a quick blog to share my recent experience of speaking at BML Munjal University (BMU), India.

I was invited there as a speaker to shed some light on GSoC and Open Source in general for their GSoCxBMU event.


![Bml campus](https://www.aryank.in/images/uploads/bml_campus.png)


The event was great, where we collectively shared the love of open source, and the students got to learn about how they can participate in it and got to know about GSoC and similar opportunities (Outreachy, LFX Mentorship, MLH Fellowship, GSoD, etc...)

Near the end, we also discussed about the GNOME Foundation and the GNOME Project, where I shared some projects to which they can contribute and details about GNOME Internships.

![gnome foundation sslide](https://www.aryank.in/images/uploads/gnome.png)

In the end, there was a short Q&A session where any queries were resolved, after which the students received some GNOME stickers which the engagement team sent me and we called it a day :D

![picture of atendees](https://www.aryank.in/images/uploads/students.png)

The event was co-organised by GDSC BMU and Sata BMU in collaboration with GNOME Users Group, Delhi - NCR.

![Organising team picture](https://www.aryank.in/images/uploads/team.png)

Let's do more events and spread the love about FOSS :D

Written by - Aryan Kaushik
